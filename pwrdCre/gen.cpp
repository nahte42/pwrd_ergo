#include "gen.h"


//Constructor will initialize all the maps
passGen::passGen(){
	//cout<<"In Constructor\n";
	//Initialization Tools
	string rows[4] = {"`1234567890-=","qwertyuiop[]\\","asdfghjkl;\'","zxcvbnm,./"};
	string shifts =   "~!@#$%^&*()_+{}|:\"<>?";
	string unshifts = "`1234567890-=[]\\;\',./";
	string popMap = "`1234567890-=qwertyuiop[]\\asdfghjkl;'zxcvbnm,./";
	string start = "`1234567890-=qwertyuiop[]\\asdfghjkl;\'zxcvbnm,./";
	string end   = "`1234567890-=qwertyuiop[]\\asdfghjkl;\'zxcvbnm,./";
	int full = start.length();
	vector<char> quad1;
	vector<char> quad2;
	vector<char> quad3;
	vector<char> quad4;
	string q1 = "`123qweasdzxc";
	string q2 = "456rtyfghvbn";
	string q3 = "789uiojklm,.";
	string q4 = "0-=[]\\;'/";
	srand(time(NULL));
	
	//Begin Initialization
	//Initialize Two Dimensional Array || KeyBoard Layout
	for(int i = 0; i < 4; i++){
		for(int j = 0; j < rows[i].length(); j++){
			if(i == 2)
				keyB[i][i+j-1] = rows[i][j];
			if(i == 3)
				keyB[i][i+j-2] = rows[i][j];
			else
			keyB[i][i+j] = rows[i][j];
		}	
	}
	keyB[0][13] = ' ';
	keyB[1][0] = ' ';
	keyB[2][0] = ' ';
	keyB[2][13] = ' ';
	keyB[3][0] = ' ';
	
	//Initialize Shift Map || ex: ! : 1
	for(int i = 0; i < shifts.length(); i++){
		shiftMap.insert(pair<char,char>(shifts[i],unshifts[i]));
	}
	
	//Initialize Distance Map with empty Map
	for(int i = 0; i < popMap.length(); i++){
		map<int, vector<char>> tmp;
		distanceMap.insert(pair<char,map<int,vector<char>>>(popMap[i],tmp));
	}
	
	//Poputlate Distance Map with Vectors of Distances
	for(int i = 0; i < full; i++){
		for(int j = 0; j < full; j++){
			int dist = getDist(keyB, start[i],end[j], shiftMap);
			if(distanceMap[start[i]].find(dist) == distanceMap[start[i]].end()){
				vector<char> tmp;
				tmp.push_back(end[j]);
				distanceMap[start[i]].insert(pair<int, vector<char>>(dist, tmp));
			}
			else{
				distanceMap[start[i]][dist].push_back(end[j]);
			}
		}
	}
	
	//Populate Quadrants Map
	for(int i = 0; i < q1.length(); i++){
		if (i == 12) {
		quad1.push_back(q1[i]);
		}
		else{
			quad1.push_back(q1[i]);
			quad2.push_back(q2[i]);
			quad3.push_back(q3[i]);
			if( i < 7)
				quad4.push_back(q4[i]);
		}
	}
	Quadrants.insert(pair<int, vector<char>>(1, quad1));
	Quadrants.insert(pair<int, vector<char>>(2, quad2));
	Quadrants.insert(pair<int, vector<char>>(3, quad3));
	Quadrants.insert(pair<int, vector<char>>(4, quad4));
	
	//initialize reverse quadrant lookup
	for(int i = 0; i < Quadrants[1].size(); i++)
		char_to_quad.insert(pair<char,int>(Quadrants[1][i], 1));
	for(int i = 0; i < Quadrants[2].size(); i++)
		char_to_quad.insert(pair<char,int>(Quadrants[2][i], 2));
	for(int i = 0; i < Quadrants[3].size(); i++)
		char_to_quad.insert(pair<char,int>(Quadrants[3][i], 3));
	for(int i = 0; i < Quadrants[4].size(); i++)
		char_to_quad.insert(pair<char,int>(Quadrants[4][i], 4));
	
	
	//Testing to Make Sure Everything Works
	/*  for(int i = 0; i < 4; i++){
		for(int j = 0; j < 14; j++){
			cout<<keyB[i][j];
		}
	  cout<<endl;
	}
	
	for(int i = 0; i < distanceMap['a'][3].size(); i++)
		cout<<distanceMap['a'][3][i]<<" ";
	cout<<endl;
	for(int i = 0; i < distanceMap['y'][1].size(); i++)
		cout<<distanceMap['y'][1][i]<<" ";
	cout<<endl;
	for(int i = 0; i < distanceMap['z'][4].size(); i++)
		cout<<distanceMap['z'][4][i]<<" ";
	cout<<endl;
	
	cout<<"Distance from a to 5 | should be 4: "<<getDist(keyB,'a','5',shiftMap)<<endl;
	cout<<"Distance from y to f | should be 2: "<<getDist(keyB,'y','f',shiftMap)<<endl;
	cout<<"Distance from W to w | should be 0: "<<getDist(keyB,'W','w',shiftMap)<<endl;
	cout<<"Distance from ` to m | should be 9: "<<getDist(keyB,'`','m',shiftMap)<<endl;
	cout<<"Distance from a to l | should be 8: "<<getDist(keyB,'a','l',shiftMap)<<endl;
	cout<<"Distance from z to m | should be 6: "<<getDist(keyB,'z','m',shiftMap)<<endl;
	cout<<"Distance from # to m | should be 6?: "<<getDist(keyB,'#','m',shiftMap)<<endl;
	cout<<"Distance from | to \\ | should be 0: "<<getDist(keyB,'|','\\',shiftMap)<<endl;
	
	cout<<"Exiting Constructor\nCalling Generate\n"<<endl;*/
	
}

int passGen::getDist(char keyB[4][14], char s, char e, map<char,char> shiftMap){
	int distance = 0;
	bool found_s, found_e;
	found_e = false;
	found_s = false;
	int srow, erow;
	int scol, ecol;
	s = unshift(s, shiftMap);
	e = unshift(e, shiftMap);

	while(!found_e || !found_s){
		for(int i = 0; i < 14; i++){
			if(keyB[0][i] == s){
				srow = 0; scol = i; found_s = true;
			}
			if(keyB[1][i] == s){
				srow = 1; scol = i; found_s = true;
			}
			if(keyB[2][i] == s){
				srow = 2; scol = i; found_s = true;
			}
			if(keyB[3][i] == s){
				srow = 3; scol = i; found_s = true;
			}
			if(keyB[0][i] == e){
				erow = 0; ecol = i; found_e = true;
			}
			if(keyB[1][i] == e){
				erow = 1; ecol = i; found_e = true;
			}
			if(keyB[2][i] == e){
				erow = 2; ecol = i; found_e = true;
			}
			if(keyB[3][i] == e){
				erow = 3; ecol = i; found_e = true;
			}

		}

	}

	int distR = abs(erow -srow);
	int distC = abs(ecol - scol) - distR;
	if(distC < 0)
		distC = 0;


	distance = distR + distC;

	return distance;
}

char passGen::unshift(char ch, map<char,char> shiftMap){
	if(ch == '|')
		return '\\';
	if(isalpha(ch))
		return tolower(ch);
	if(isdigit(ch))
		return ch;
	else{
		if(shiftMap.find(ch) == shiftMap.end())
			return ch;
		else
			return shiftMap[ch];
	}
}

char passGen::shiftIt(char ch, map<char,char>shiftMap){
  if(ch == '\\')
    return '|';
  if(isalpha(ch))
    return toupper(ch);
  else{
    string locate = "~!@#$%^&*()_+{}:\"<>?";
    for(auto it = shiftMap.begin(); it != shiftMap.end(); it++){
      if(it->second == ch)
        return it->first;
    }
  }
  return '\0';
}

void passGen::generate(){
	//First we need the length of the password
	//Then we need the first character
	//Then we need to pick the next characters based on provided metrics
	  srand(time(0));
	  //string filename = "GenPass.txt";
	  //fstream file;
	  //file.open(filename.c_str(), ios::out);
	  string pwrd = "";
	  int pwds = 0;
	  int length, nextCh, shift, dist;
	  
	  while(pwds < 10){
		length = pickLength();
		pwrd += pickFirstChar();
		//cout<<length<<endl;
		//cout<<pwrd<<endl;
		for(int i = 1; i < length; i++){
			pwrd+=pickNextChar(pwrd[pwrd.length()-1]);
			if((rand()%100+1) <= 10)
				pwrd[i] = shiftIt(pwrd[i], shiftMap);
			//cout<<pwrd<<endl;
		}
		
		cout<<pwrd<<endl;
		pwrd = "";
		pwds++;
	  }
	 // file.close();

}


char passGen::pickFirstChar(){
	int pick = rand()%100+1;
	//	cout<<"pick: "<<pick<<endl;
	// 1 - 37 Q1| 37 - 66 Q2| 66 - 94 Q3| 94 - 100 Q4|  
	//Slightly rounded down
	
	if(pick >= 1 && pick < 44){
		return Quadrants[1][rand()%Quadrants[1].size()];
	}
	if(pick >= 44 && pick < 72){
		return Quadrants[2][rand()%Quadrants[2].size()];
	}
	if(pick >= 72 && pick < 92){
		return Quadrants[3][rand()%Quadrants[3].size()];
	}
	else{
		return Quadrants[4][rand()%Quadrants[4].size()];
	}

	
}
int passGen::pickLength(){
	int length = rand()%100+1;
	if(length >=1 && length <= 3)
		return 5;
	if(length>=4 && length <= 23)
		return 6;
	if(length >= 24 && length <= 40)
		return 7;
	if(length >= 40 && length <= 70)
		return 8;
	if(length >= 71 && length <= 80)
		return 9;
	if(length >= 81 && length <= 88)
		return 10;
	if(length >= 89 && length <= 92)
		return 11;
	if(length >= 93 && length <=94)
		return 12;
	if(length >= 95 && length <= 96)
		return 13;
	if(length >= 97 && length <= 100)
		return 14;
	return 0;
}
char passGen::pickNextChar(char curr){
	//To pick the next characer, we take current quadrant
	//From there we pick the next most likely quadrant, and from there, we pick a distance and check if there exists a distance from that quadrant, and randomly select a character from there.
	//We also need to To include section and quadrants liklihood to be based on section(FOR LATER DEVELOPMENT)
	int pick = rand()%100+1;
	int next_quad;
	int distance = get_distance();
	vector<int> last_distance;
	last_distance.push_back(distance);//So we dont keep trying distances we have used if they have failed previously
	if(char_to_quad[curr] == 1){
		if(pick >=1 && pick <= 38){
			next_quad = 1;
			vector<char> choices;
			while(choices.size()== 0){
				for(int i = 0; i < distanceMap[curr][distance].size(); i++)
					if(char_to_quad[distanceMap[curr][distance][i]] == next_quad)
						choices.push_back(distanceMap[curr][distance][i]);
				if(choices.size() == 0){
					distance = get_distance();
					while(exists(last_distance, distance)){
						distance = get_distance();
					}
				}
			}
			return choices[rand()%choices.size()];
		}
		if(pick >=39 && pick <= 69){
			next_quad = 2;
			vector<char> choices;
			while(choices.size()== 0){
				for(int i = 0; i < distanceMap[curr][distance].size(); i++)
					if(char_to_quad[distanceMap[curr][distance][i]] == next_quad)
						choices.push_back(distanceMap[curr][distance][i]);
				if(choices.size() == 0){
					distance = get_distance();
					while(exists(last_distance, distance)){
						distance = get_distance();
					}
				}
			}
			return choices[rand()%choices.size()];

			
		}
		if(pick >=70 && pick <= 94){
			next_quad = 3;
			vector<char> choices;
			while(choices.size()== 0){
				for(int i = 0; i < distanceMap[curr][distance].size(); i++)
					if(char_to_quad[distanceMap[curr][distance][i]] == next_quad)
						choices.push_back(distanceMap[curr][distance][i]);
				if(choices.size() == 0){
					distance = get_distance();
					while(exists(last_distance, distance)){
						distance = get_distance();
					}
				}
			}
			return choices[rand()%choices.size()];
			
		}
		if(pick >=95 && pick <= 100){
			next_quad = 4;
			vector<char> choices;
			while(choices.size()== 0){
				for(int i = 0; i < distanceMap[curr][distance].size(); i++)
					if(char_to_quad[distanceMap[curr][distance][i]] == next_quad)
						choices.push_back(distanceMap[curr][distance][i]);
				if(choices.size() == 0){
					distance = get_distance();
					while(exists(last_distance, distance)){
						distance = get_distance();
					}
				}
			}
			return choices[rand()%choices.size()];
			
		}
	}
	if(char_to_quad[curr] == 2){
		if(pick >=1 && pick <= 39){
			next_quad = 1;
			vector<char> choices;
			while(choices.size()== 0){
				for(int i = 0; i < distanceMap[curr][distance].size(); i++)
					if(char_to_quad[distanceMap[curr][distance][i]] == next_quad)
						choices.push_back(distanceMap[curr][distance][i]);
				if(choices.size() == 0){
					distance = get_distance();
					while(exists(last_distance, distance)){
						distance = get_distance();
					}
				}
			}
			return choices[rand()%choices.size()];
			
		}
		if(pick >=40 && pick <= 73){
			next_quad = 2;
			vector<char> choices;
			while(choices.size()== 0){
				for(int i = 0; i < distanceMap[curr][distance].size(); i++)
					if(char_to_quad[distanceMap[curr][distance][i]] == next_quad)
						choices.push_back(distanceMap[curr][distance][i]);
				if(choices.size() == 0){
					distance = get_distance();
					while(exists(last_distance, distance)){
						distance = get_distance();
					}
				}
			}
			return choices[rand()%choices.size()];
			
		}
		if(pick >=74 && pick <= 95){
			next_quad = 3;
			vector<char> choices;
			while(choices.size()== 0){
				for(int i = 0; i < distanceMap[curr][distance].size(); i++)
					if(char_to_quad[distanceMap[curr][distance][i]] == next_quad)
						choices.push_back(distanceMap[curr][distance][i]);
				if(choices.size() == 0){
					distance = get_distance();
					while(exists(last_distance, distance)){
						distance = get_distance();
					}
				}
			}
			return choices[rand()%choices.size()];
			
		}
		if(pick >=96 && pick <= 100){
			next_quad = 4;
			vector<char> choices;
			while(choices.size()== 0){
				for(int i = 0; i < distanceMap[curr][distance].size(); i++)
					if(char_to_quad[distanceMap[curr][distance][i]] == next_quad)
						choices.push_back(distanceMap[curr][distance][i]);
				if(choices.size() == 0){
					distance = get_distance();
					while(exists(last_distance, distance)){
						distance = get_distance();
					}
				}
			}
			return choices[rand()%choices.size()];
			
		}
	}
	if(char_to_quad[curr] == 3){
		if(pick >=1 && pick <= 39){
			next_quad = 1;
			vector<char> choices;
			while(choices.size()== 0){
				for(int i = 0; i < distanceMap[curr][distance].size(); i++)
					if(char_to_quad[distanceMap[curr][distance][i]] == next_quad)
						choices.push_back(distanceMap[curr][distance][i]);
				if(choices.size() == 0){
					distance = get_distance();
					while(exists(last_distance, distance)){
						distance = get_distance();
					}
				}
			}
			return choices[rand()%choices.size()];
			
		}
		if(pick >=40 && pick <= 70){
			next_quad = 2;
			vector<char> choices;
			while(choices.size()== 0){
				for(int i = 0; i < distanceMap[curr][distance].size(); i++)
					if(char_to_quad[distanceMap[curr][distance][i]] == next_quad)
						choices.push_back(distanceMap[curr][distance][i]);
				if(choices.size() == 0){
					distance = get_distance();
					while(exists(last_distance, distance)){
						distance = get_distance();
					}
				}
			}
			return choices[rand()%choices.size()];
			
		}
		if(pick >=71 && pick <= 93){
			next_quad = 3;
			vector<char> choices;
			while(choices.size()== 0){
				for(int i = 0; i < distanceMap[curr][distance].size(); i++)
					if(char_to_quad[distanceMap[curr][distance][i]] == next_quad)
						choices.push_back(distanceMap[curr][distance][i]);
				if(choices.size() == 0){
					distance = get_distance();
					while(exists(last_distance, distance)){
						distance = get_distance();
					}
				}
			}
			return choices[rand()%choices.size()];
			
		}
		if(pick >=93 && pick <= 100){
			next_quad = 4;
			vector<char> choices;
			while(choices.size()== 0){
				for(int i = 0; i < distanceMap[curr][distance].size(); i++)
					if(char_to_quad[distanceMap[curr][distance][i]] == next_quad)
						choices.push_back(distanceMap[curr][distance][i]);
				if(choices.size() == 0){
					distance = get_distance();
					while(exists(last_distance, distance)){
						distance = get_distance();
					}
				}
			}
			return choices[rand()%choices.size()];

		}
	}
	if(char_to_quad[curr] == 4){
		if(pick >=1 && pick <= 37){
			next_quad = 1;
			vector<char> choices;
			while(choices.size()== 0){
				for(int i = 0; i < distanceMap[curr][distance].size(); i++)
					if(char_to_quad[distanceMap[curr][distance][i]] == next_quad)
						choices.push_back(distanceMap[curr][distance][i]);
				if(choices.size() == 0){
					distance = get_distance();
					while(exists(last_distance, distance)){
						distance = get_distance();
					}
				}
			}
			return choices[rand()%choices.size()];
		}
		if(pick >=38 && pick <= 66){
			next_quad = 2;
			vector<char> choices;
			while(choices.size()== 0){
				for(int i = 0; i < distanceMap[curr][distance].size(); i++)
					if(char_to_quad[distanceMap[curr][distance][i]] == next_quad)
						choices.push_back(distanceMap[curr][distance][i]);
				if(choices.size() == 0){
					distance = get_distance();
					while(exists(last_distance, distance)){
						distance = get_distance();
					}
				}
			}
			return choices[rand()%choices.size()];
		}
		if(pick >=67 && pick <= 92){
			next_quad = 3;
			vector<char> choices;
			while(choices.size()== 0){
				for(int i = 0; i < distanceMap[curr][distance].size(); i++)
					if(char_to_quad[distanceMap[curr][distance][i]] == next_quad)
						choices.push_back(distanceMap[curr][distance][i]);
				if(choices.size() == 0){
					distance = get_distance();
					while(exists(last_distance, distance)){
						distance = get_distance();
					}
				}
			}
			return choices[rand()%choices.size()];
			
		}
		if(pick >=93 && pick <= 100){
			next_quad = 4;
			vector<char> choices;
			while(choices.size()== 0){
				for(int i = 0; i < distanceMap[curr][distance].size(); i++)
					if(char_to_quad[distanceMap[curr][distance][i]] == next_quad)
						choices.push_back(distanceMap[curr][distance][i]);
				if(choices.size() == 0){
					distance = get_distance();
					while(exists(last_distance, distance)){
						distance = get_distance();
					}
				}
			}
			return choices[rand()%choices.size()];
		}
	}	
	return 'a';
}

bool passGen::exists(vector<int> distances, int find){
	for(int i = 0; i < distances.size(); i++)
		if(distances[i] == find)
			return true;
	return false;
}

int passGen::get_distance(){
	int pick = rand()%100+1;
	
	if(pick >= 1 && pick <=7)
		return 0;
	if(pick >= 8 && pick <=28)
		return 1;
	if(pick >= 29 && pick <= 49)
		return 2;
	if(pick >= 50 && pick <= 62)
		return 3;
	if(pick >= 63 && pick <= 72)
		return 4;
	if(pick >= 73 && pick <= 81)
		return 5;
	if(pick >= 82 && pick <= 88)
		return 6;
	if(pick >= 89 && pick <= 93)
		return 7;
	if(pick >= 94 && pick <= 97)
		return 8;
	if(pick == 98 || pick == 99)
		return 9;
	if(pick == 100){
		int gnu_pick = rand()%4;
		if(gnu_pick == 0)
			return 10;
		if(gnu_pick == 1)
			return 11;
		if(gnu_pick == 2);
			return 12;
		if(gnu_pick == 3);
			return 13;
	}	
	
	return 0;
}