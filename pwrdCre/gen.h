#include<iostream>
#include<vector>
#include<string>
#include<cstdlib>
#include<time.h>
#include<fstream>
#include<map>

using namespace std;


#ifndef GEN_H
#define GEN_H

class passGen{
	private:
		char keyB[4][14];
		map<char, char> shiftMap;
		map<char,map<int,vector<char>>> distanceMap;
		map<int, vector<char>> Quadrants;
		map<char, int> char_to_quad;
	public:
		passGen();
		int 	getDist(char[4][14], char s, char e, map<char,char> shiftMap);
		char 	unshift(char ch, map<char,char> shiftMap);
		char 	shiftIt(char ch, map<char,char>shiftMap);
		void 	generate();
		char 	pickFirstChar();
		int 	pickLength();
		char 	pickNextChar(char curr);
		int		get_distance();
		bool 	exists(vector<int> distances, int find);
};


#endif