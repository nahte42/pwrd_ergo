
pwd = open('allPass.txt', 'r')
pwdList = pwd.readlines()

pwdLength = {}

for passwd in pwdList:
    length = int(len(passwd))-1
    if length not in pwdLength.keys():
        pwdLength[length] = 1
    else:
        pwdLength[length] = pwdLength[length] + 1

for x in pwdLength:
    print("occurrences of password length ", x,": ", pwdLength[x])

for x in pwdLength:
    print("percentage use of password length ", x,": ", pwdLength[x]/int(len(pwdList)))
