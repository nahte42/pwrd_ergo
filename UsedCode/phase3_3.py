import math
#The following code will be used to 
#to find patterns in groups of 4
#among the list of patterns

patterns = open('patterns3.txt', 'r')
pattList = patterns.readlines()
pattAnyl = open('patternA3_3.txt', 'w+')


#Empty dictionary to store patterns
pattDict = {}

for pattern in pattList:
    size = int(len(pattern))
    sect = math.ceil(size/4)
    for x in range(sect):
        if x == sect-1:
            if int(len(pattern[x*4:])) == 1:
                continue
            if pattern[x*4:int(len(pattern)-1)] not in pattDict.keys():
                pattDict[pattern[x*4:int(len(pattern)-1)]] = 1
            else:
                pattDict[pattern[x*4:int(len(pattern)-1)]] = pattDict[pattern[x*4:int(len(pattern)-1)]] + 1
        if pattern[x*4:(x*4)+4] not in pattDict.keys():
            pattDict[pattern[x*4:(x*4)+4]] = 1
        else:
            pattDict[pattern[x*4:(x*4)+4]] = pattDict[pattern[x*4:(x*4)+4]] +  1
    
for x in pattDict:
    pattAnyl.write(x+"\t"+str(pattDict[x])+"\n")    
    print(x)