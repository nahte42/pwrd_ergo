#Phase 4, running the ame processes and algorithms but we are now dividing the keyboard into 8 quadrants to gather
#Shift key usage and placement.
#Ethan Jones
#December 29th, 2019

#initialize empty dictionary
Quadrants = {}

#populate dictionary
pop_dic = ["`123qweasdzxc", "456rtyfghvbn",  "789uiojklm,.",  "0-=p[]\\;\'/", "~!@#QWEASDZXC", "$%^RTYFGHVBN","&*(UIOJKLM<>",")_+P{}|:\"?"]
for x in range(len(pop_dic)):
    for ch in pop_dic[x]:
        if ch not in Quadrants.keys():
            Quadrants[ch] = x+1
Quadrants [' '] = '2'

'''
Test That Dictionary Was popluated Correctly
print("q: ", Quadrants['q'])
print("Q: ", Quadrants['Q'])
print("5: ", Quadrants['5'])
print("%: ", Quadrants['%'])
print("i: ", Quadrants['i'])
print("I: ", Quadrants['I'])
print("\\: ", Quadrants['\\'])
print("|: ", Quadrants['|'])
'''
#Create a list of passwords to be converted into qudarant patterns
'''
pwrds = open('ry.txt', 'r')
pwrd_list = pwrds.readlines()
pattern4 = open('a.txt', 'w+')

for pwd in pwrd_list:
    pattern = ""
    for lett in pwd:
        try:
            if(lett != '\n'):
                pattern = pattern + str(Quadrants[lett])
        except:
            continue
    pattern4.write(pattern + '\n')

pattern4.close()
'''
#We have compiled the data into easy to use quadrant data, we will now perform the analysis.
pattern4 = open('allPatt8.txt', 'r')
pattern_list = pattern4.readlines()

dictFirst_End = {'1': [0,0,0,0,0,0,0,0],'2': [0,0,0,0,0,0,0,0],'3': [0,0,0,0,0,0,0,0],'4': [0,0,0,0,0,0,0,0],\
                 '5': [0,0,0,0,0,0,0,0],'6': [0,0,0,0,0,0,0,0],'7': [0,0,0,0,0,0,0,0],'8': [0,0,0,0,0,0,0,0]}
dictNext_Quad = {'1': [0,0,0,0,0,0,0,0],'2': [0,0,0,0,0,0,0,0],'3': [0,0,0,0,0,0,0,0],'4': [0,0,0,0,0,0,0,0],\
                 '5': [0,0,0,0,0,0,0,0],'6': [0,0,0,0,0,0,0,0],'7': [0,0,0,0,0,0,0,0],'8': [0,0,0,0,0,0,0,0]}
dicStart_Quad = {'1':0, '2':0, '3':0, '4':0,'5':0, '6':0, '7':0, '8':0}
dicEndin_Quad = {'1':0, '2':0, '3':0, '4':0,'5':0, '6':0, '7':0, '8':0}
dicTotal_Usag = {'1':0, '2':0, '3':0, '4':0,'5':0, '6':0, '7':0, '8':0}

for pattern in pattern_list:
    start = pattern[0]
    end = pattern[int(len(pattern)) -2]
    if end == '\n':
        continue
    dictFirst_End[start][int(end)-1] = dictFirst_End[start][int(end)-1] + 1
    dicStart_Quad[start] = dicStart_Quad[start] + 1
    dicEndin_Quad[end] = dicEndin_Quad[end]+1
    for num in range(int(len(pattern))-2):
        curr = pattern[num]
        next = pattern[num+1]
        inext = int(next)
        dictNext_Quad[curr][inext-1] = dictNext_Quad[curr][inext-1] + 1
        dicTotal_Usag[curr] = dicTotal_Usag[curr] + 1


#Compute Totals
total = int(len(pattern_list))
totalNext = [0,0,0,0,0,0,0,0]
totalFnEn = [0,0,0,0,0,0,0,0]
totalStrt = dicStart_Quad['1'] + dicStart_Quad['2'] + dicStart_Quad['3'] + dicStart_Quad['4'] +\
            dicStart_Quad['5'] + dicStart_Quad['6'] + dicStart_Quad['7'] + dicStart_Quad['8']
totalEndg = dicEndin_Quad['1'] + dicEndin_Quad['2'] + dicEndin_Quad['3'] + dicEndin_Quad['4'] +\
            dicEndin_Quad['5'] + dicEndin_Quad['6'] + dicEndin_Quad['7'] + dicEndin_Quad['8']
totalLett = dicTotal_Usag['1'] + dicTotal_Usag['2'] + dicTotal_Usag['3'] + dicTotal_Usag['4'] +\
            dicTotal_Usag['5'] + dicTotal_Usag['6'] + dicTotal_Usag['7'] + dicTotal_Usag['8']

totalNext[0] = dictNext_Quad['1'][0] + dictNext_Quad['1'][1] + dictNext_Quad['1'][2] + dictNext_Quad['1'][3] +\
               dictNext_Quad['1'][4] + dictNext_Quad['1'][5] + dictNext_Quad['1'][6] + dictNext_Quad['1'][7]

totalNext[1] = dictNext_Quad['2'][0] + dictNext_Quad['2'][1] + dictNext_Quad['2'][2] + dictNext_Quad['2'][3] +\
               dictNext_Quad['2'][4] + dictNext_Quad['2'][5] + dictNext_Quad['2'][6] + dictNext_Quad['2'][7]

totalNext[2] = dictNext_Quad['3'][0] + dictNext_Quad['3'][1] + dictNext_Quad['3'][2] + dictNext_Quad['3'][3] +\
               dictNext_Quad['3'][4] + dictNext_Quad['3'][5] + dictNext_Quad['3'][6] + dictNext_Quad['3'][7]

totalNext[3] = dictNext_Quad['4'][0] + dictNext_Quad['4'][1] + dictNext_Quad['4'][2] + dictNext_Quad['4'][3] +\
               dictNext_Quad['4'][4] + dictNext_Quad['4'][5] + dictNext_Quad['4'][6] + dictNext_Quad['4'][7]

totalNext[4] = dictNext_Quad['4'][0] + dictNext_Quad['4'][1] + dictNext_Quad['4'][2] + dictNext_Quad['4'][3] +\
               dictNext_Quad['4'][4] + dictNext_Quad['4'][5] + dictNext_Quad['4'][6] + dictNext_Quad['4'][7]

totalNext[5] = dictNext_Quad['5'][0] + dictNext_Quad['5'][1] + dictNext_Quad['5'][2] + dictNext_Quad['5'][3] +\
               dictNext_Quad['5'][4] + dictNext_Quad['5'][5] + dictNext_Quad['5'][6] + dictNext_Quad['5'][7]

totalNext[6] = dictNext_Quad['6'][0] + dictNext_Quad['6'][1] + dictNext_Quad['6'][2] + dictNext_Quad['6'][3] +\
               dictNext_Quad['6'][4] + dictNext_Quad['6'][5] + dictNext_Quad['6'][6] + dictNext_Quad['6'][7]

totalNext[7] = dictNext_Quad['7'][0] + dictNext_Quad['7'][1] + dictNext_Quad['7'][2] + dictNext_Quad['7'][3] +\
               dictNext_Quad['7'][4] + dictNext_Quad['7'][5] + dictNext_Quad['7'][6] + dictNext_Quad['7'][7]

totalFnEn[0] = dictFirst_End['1'][0] + dictFirst_End['1'][1] + dictFirst_End['1'][2] + dictFirst_End['1'][3] +\
               dictFirst_End['1'][4] + dictFirst_End['1'][5] + dictFirst_End['1'][6] + dictFirst_End['1'][7]

totalFnEn[1] = dictFirst_End['2'][0] + dictFirst_End['2'][1] + dictFirst_End['2'][2] + dictFirst_End['2'][3] +\
               dictFirst_End['2'][4] + dictFirst_End['2'][5] + dictFirst_End['2'][6] + dictFirst_End['2'][7]

totalFnEn[2] = dictFirst_End['3'][0] + dictFirst_End['3'][1] + dictFirst_End['3'][2] + dictFirst_End['3'][3] +\
               dictFirst_End['3'][4] + dictFirst_End['3'][5] + dictFirst_End['3'][6] + dictFirst_End['3'][7]

totalFnEn[3] = dictFirst_End['4'][0] + dictFirst_End['4'][1] + dictFirst_End['4'][2] + dictFirst_End['4'][3] +\
               dictFirst_End['4'][4] + dictFirst_End['4'][5] + dictFirst_End['4'][6] + dictFirst_End['4'][7]

totalFnEn[4] = dictFirst_End['4'][0] + dictFirst_End['4'][1] + dictFirst_End['4'][2] + dictFirst_End['4'][3] +\
               dictFirst_End['4'][4] + dictFirst_End['4'][5] + dictFirst_End['4'][6] + dictFirst_End['4'][7]

totalFnEn[5] = dictFirst_End['5'][0] + dictFirst_End['5'][1] + dictFirst_End['5'][2] + dictFirst_End['5'][3] +\
               dictFirst_End['5'][4] + dictFirst_End['5'][5] + dictFirst_End['5'][6] + dictFirst_End['5'][7]

totalFnEn[6] = dictFirst_End['6'][0] + dictFirst_End['6'][1] + dictFirst_End['6'][2] + dictFirst_End['6'][3] +\
               dictFirst_End['6'][4] + dictFirst_End['6'][5] + dictFirst_End['6'][6] + dictFirst_End['6'][7]

totalFnEn[7] = dictFirst_End['7'][0] + dictFirst_End['7'][1] + dictFirst_End['7'][2] + dictFirst_End['7'][3] +\
               dictFirst_End['7'][4] + dictFirst_End['7'][5] + dictFirst_End['7'][6] + dictFirst_End['7'][7]



#-------------------------------------------------------------------------
#Total use
print("Total use of quadrant 1: ", dicTotal_Usag['1'])
print("Total use of quadrant 2: ", dicTotal_Usag['2'])
print("Total use of quadrant 3: ", dicTotal_Usag['3'])
print("Total use of quadrant 4: ", dicTotal_Usag['4'])
print("Total use of quadrant 5: ", dicTotal_Usag['5'])
print("Total use of quadrant 6: ", dicTotal_Usag['6'])
print("Total use of quadrant 7: ", dicTotal_Usag['7'])
print("Total use of quadrant 8: ", dicTotal_Usag['8'])
print("Total percent use of quadrant 1: ", dicTotal_Usag['1']/totalLett)
print("Total percent use of quadrant 2: ", dicTotal_Usag['2']/totalLett)
print("Total percent use of quadrant 3: ", dicTotal_Usag['3']/totalLett)
print("Total percent use of quadrant 4: ", dicTotal_Usag['4']/totalLett)
print("Total percent use of quadrant 5: ", dicTotal_Usag['5']/totalLett)
print("Total percent use of quadrant 6: ", dicTotal_Usag['6']/totalLett)
print("Total percent use of quadrant 7: ", dicTotal_Usag['7']/totalLett)
print("Total percent use of quadrant 8: ", dicTotal_Usag['8']/totalLett)

#Total Start
print("Total Starting of Quadrant 1: ", dicStart_Quad['1'])
print("Total Starting of Quadrant 2: ", dicStart_Quad['2'])
print("Total Starting of Quadrant 3: ", dicStart_Quad['3'])
print("Total Starting of Quadrant 4: ", dicStart_Quad['4'])
print("Total Starting of Quadrant 5: ", dicStart_Quad['5'])
print("Total Starting of Quadrant 6: ", dicStart_Quad['6'])
print("Total Starting of Quadrant 7: ", dicStart_Quad['7'])
print("Total Starting of Quadrant 8: ", dicStart_Quad['8'])
print("Total Percent of Starting of Quadrant 1: ", dicStart_Quad['1']/totalStrt)
print("Total Percent of Starting of Quadrant 2: ", dicStart_Quad['2']/totalStrt)
print("Total Percent of Starting of Quadrant 3: ", dicStart_Quad['3']/totalStrt)
print("Total Percent of Starting of Quadrant 4: ", dicStart_Quad['4']/totalStrt)
print("Total Percent of Starting of Quadrant 5: ", dicStart_Quad['5']/totalStrt)
print("Total Percent of Starting of Quadrant 6: ", dicStart_Quad['6']/totalStrt)
print("Total Percent of Starting of Quadrant 7: ", dicStart_Quad['7']/totalStrt)
print("Total Percent of Starting of Quadrant 8: ", dicStart_Quad['8']/totalStrt)

#Total End
print("Total Ending of Quadrant 1: ", dicEndin_Quad['1'])
print("Total Ending of Quadrant 2: ", dicEndin_Quad['2'])
print("Total Ending of Quadrant 3: ", dicEndin_Quad['3'])
print("Total Ending of Quadrant 4: ", dicEndin_Quad['4'])
print("Total Ending of Quadrant 5: ", dicEndin_Quad['5'])
print("Total Ending of Quadrant 6: ", dicEndin_Quad['6'])
print("Total Ending of Quadrant 7: ", dicEndin_Quad['7'])
print("Total Ending of Quadrant 8: ", dicEndin_Quad['8'])
print("Total Percent of Ending of Quadrant 1: ", dicEndin_Quad['1']/totalEndg)
print("Total Percent of Ending of Quadrant 2: ", dicEndin_Quad['2']/totalEndg)
print("Total Percent of Ending of Quadrant 3:", dicEndin_Quad['3']/totalEndg)
print("Total Percent of Ending of Quadrant 4: ", dicEndin_Quad['4']/totalEndg)
print("Total Percent of Ending of Quadrant 5: ", dicEndin_Quad['5']/totalEndg)
print("Total Percent of Ending of Quadrant 6: ", dicEndin_Quad['6']/totalEndg)
print("Total Percent of Ending of Quadrant 7: ", dicEndin_Quad['7']/totalEndg)
print("Total Percent of Ending of Quadrant 8: ", dicEndin_Quad['8']/totalEndg)

#Total Next
print("Total of Quadrant 1 to 1: ",dictNext_Quad['1'][0])
print("Total of Quadrant 1 to 2: ",dictNext_Quad['1'][1])
print("Total of Quadrant 1 to 3: ",dictNext_Quad['1'][2])
print("Total of Quadrant 1 to 4: ",dictNext_Quad['1'][3])
print("Total of Quadrant 1 to 5: ",dictNext_Quad['1'][4])
print("Total of Quadrant 1 to 6: ",dictNext_Quad['1'][5])
print("Total of Quadrant 1 to 7: ",dictNext_Quad['1'][6])
print("Total of Quadrant 1 to 8: ",dictNext_Quad['1'][7])

print("Total of Quadrant 2 to 1: ",dictNext_Quad['2'][0])
print("Total of Quadrant 2 to 2: ",dictNext_Quad['2'][1])
print("Total of Quadrant 2 to 3: ",dictNext_Quad['2'][2])
print("Total of Quadrant 2 to 4: ",dictNext_Quad['2'][3])
print("Total of Quadrant 2 to 5: ",dictNext_Quad['2'][4])
print("Total of Quadrant 2 to 6: ",dictNext_Quad['2'][5])
print("Total of Quadrant 2 to 7: ",dictNext_Quad['2'][6])
print("Total of Quadrant 2 to 8: ",dictNext_Quad['2'][7])

print("Total of Quadrant 3 to 1: ",dictNext_Quad['3'][0])
print("Total of Quadrant 3 to 2: ",dictNext_Quad['3'][1])
print("Total of Quadrant 3 to 3: ",dictNext_Quad['3'][2])
print("Total of Quadrant 3 to 4: ",dictNext_Quad['3'][3])
print("Total of Quadrant 3 to 1: ",dictNext_Quad['3'][4])
print("Total of Quadrant 3 to 2: ",dictNext_Quad['3'][5])
print("Total of Quadrant 3 to 3: ",dictNext_Quad['3'][6])
print("Total of Quadrant 3 to 4: ",dictNext_Quad['3'][7])

print("Total of Quadrant 4 to 1: ",dictNext_Quad['4'][0])
print("Total of Quadrant 4 to 2: ",dictNext_Quad['4'][1])
print("Total of Quadrant 4 to 3: ",dictNext_Quad['4'][2])
print("Total of Quadrant 4 to 4: ",dictNext_Quad['4'][3])
print("Total of Quadrant 4 to 5: ",dictNext_Quad['4'][4])
print("Total of Quadrant 4 to 6: ",dictNext_Quad['4'][5])
print("Total of Quadrant 4 to 7: ",dictNext_Quad['4'][6])
print("Total of Quadrant 4 to 8: ",dictNext_Quad['4'][7])

print("Total of Quadrant 5 to 1: ",dictNext_Quad['5'][0])
print("Total of Quadrant 5 to 2: ",dictNext_Quad['5'][1])
print("Total of Quadrant 5 to 3: ",dictNext_Quad['5'][2])
print("Total of Quadrant 5 to 4: ",dictNext_Quad['5'][3])
print("Total of Quadrant 5 to 5: ",dictNext_Quad['5'][4])
print("Total of Quadrant 5 to 6: ",dictNext_Quad['5'][5])
print("Total of Quadrant 5 to 7: ",dictNext_Quad['5'][6])
print("Total of Quadrant 5 to 8: ",dictNext_Quad['5'][7])

print("Total of Quadrant 6 to 1: ",dictNext_Quad['6'][0])
print("Total of Quadrant 6 to 2: ",dictNext_Quad['6'][1])
print("Total of Quadrant 6 to 3: ",dictNext_Quad['6'][2])
print("Total of Quadrant 6 to 4: ",dictNext_Quad['6'][3])
print("Total of Quadrant 6 to 5: ",dictNext_Quad['6'][4])
print("Total of Quadrant 6 to 6: ",dictNext_Quad['6'][5])
print("Total of Quadrant 6 to 7: ",dictNext_Quad['6'][6])
print("Total of Quadrant 6 to 8: ",dictNext_Quad['6'][7])

print("Total of Quadrant 7 to 1: ",dictNext_Quad['7'][0])
print("Total of Quadrant 7 to 2: ",dictNext_Quad['7'][1])
print("Total of Quadrant 7 to 3: ",dictNext_Quad['7'][2])
print("Total of Quadrant 7 to 4: ",dictNext_Quad['7'][3])
print("Total of Quadrant 7 to 5: ",dictNext_Quad['7'][4])
print("Total of Quadrant 7 to 6: ",dictNext_Quad['7'][5])
print("Total of Quadrant 7 to 7: ",dictNext_Quad['7'][6])
print("Total of Quadrant 7 to 8: ",dictNext_Quad['7'][7])

print("Total of Quadrant 8 to 1: ",dictNext_Quad['8'][0])
print("Total of Quadrant 8 to 2: ",dictNext_Quad['8'][1])
print("Total of Quadrant 8 to 3: ",dictNext_Quad['8'][2])
print("Total of Quadrant 8 to 4: ",dictNext_Quad['8'][3])
print("Total of Quadrant 8 to 5: ",dictNext_Quad['8'][4])
print("Total of Quadrant 8 to 6: ",dictNext_Quad['8'][5])
print("Total of Quadrant 8 to 7: ",dictNext_Quad['8'][6])
print("Total of Quadrant 8 to 8: ",dictNext_Quad['8'][7])

print("Total Percent of Quadrant 1 to 1: ",dictNext_Quad['1'][0]/totalNext[0])
print("Total Percent of Quadrant 1 to 2: ",dictNext_Quad['1'][1]/totalNext[0])
print("Total Percent of Quadrant 1 to 3: ",dictNext_Quad['1'][2]/totalNext[0])
print("Total Percent of Quadrant 1 to 4: ",dictNext_Quad['1'][3]/totalNext[0])
print("Total Percent of Quadrant 1 to 5: ",dictNext_Quad['1'][4]/totalNext[0])
print("Total Percent of Quadrant 1 to 6: ",dictNext_Quad['1'][5]/totalNext[0])
print("Total Percent of Quadrant 1 to 7: ",dictNext_Quad['1'][6]/totalNext[0])
print("Total Percent of Quadrant 1 to 8: ",dictNext_Quad['1'][7]/totalNext[0])

print("Total Percent of Quadrant 2 to 1: ",dictNext_Quad['2'][0]/totalNext[1])
print("Total Percent of Quadrant 2 to 2: ",dictNext_Quad['2'][1]/totalNext[1])
print("Total Percent of Quadrant 2 to 3: ",dictNext_Quad['2'][2]/totalNext[1])
print("Total Percent of Quadrant 2 to 4: ",dictNext_Quad['2'][3]/totalNext[1])
print("Total Percent of Quadrant 2 to 5: ",dictNext_Quad['2'][4]/totalNext[1])
print("Total Percent of Quadrant 2 to 6: ",dictNext_Quad['2'][5]/totalNext[1])
print("Total Percent of Quadrant 2 to 7: ",dictNext_Quad['2'][6]/totalNext[1])
print("Total Percent of Quadrant 2 to 8: ",dictNext_Quad['2'][7]/totalNext[1])

print("Total Percent of Quadrant 3 to 1: ",dictNext_Quad['3'][0]/totalNext[2])
print("Total Percent of Quadrant 3 to 2: ",dictNext_Quad['3'][1]/totalNext[2])
print("Total Percent of Quadrant 3 to 3: ",dictNext_Quad['3'][2]/totalNext[2])
print("Total Percent of Quadrant 3 to 4: ",dictNext_Quad['3'][3]/totalNext[2])
print("Total Percent of Quadrant 3 to 5: ",dictNext_Quad['3'][4]/totalNext[2])
print("Total Percent of Quadrant 3 to 6: ",dictNext_Quad['3'][5]/totalNext[2])
print("Total Percent of Quadrant 3 to 7: ",dictNext_Quad['3'][6]/totalNext[2])
print("Total Percent of Quadrant 3 to 8: ",dictNext_Quad['3'][7]/totalNext[2])

print("Total Percent of Quadrant 4 to 1: ",dictNext_Quad['4'][0]/totalNext[3])
print("Total Percent of Quadrant 4 to 2: ",dictNext_Quad['4'][1]/totalNext[3])
print("Total Percent of Quadrant 4 to 3: ",dictNext_Quad['4'][2]/totalNext[3])
print("Total Percent of Quadrant 4 to 4: ",dictNext_Quad['4'][3]/totalNext[3])
print("Total Percent of Quadrant 4 to 5: ",dictNext_Quad['4'][4]/totalNext[3])
print("Total Percent of Quadrant 4 to 6: ",dictNext_Quad['4'][5]/totalNext[3])
print("Total Percent of Quadrant 4 to 7: ",dictNext_Quad['4'][6]/totalNext[3])
print("Total Percent of Quadrant 4 to 8: ",dictNext_Quad['4'][7]/totalNext[3])

print("Total Percent of Quadrant 5 to 1: ",dictNext_Quad['5'][0]/totalNext[3])
print("Total Percent of Quadrant 5 to 2: ",dictNext_Quad['5'][1]/totalNext[3])
print("Total Percent of Quadrant 5 to 3: ",dictNext_Quad['5'][2]/totalNext[3])
print("Total Percent of Quadrant 5 to 4: ",dictNext_Quad['5'][3]/totalNext[3])
print("Total Percent of Quadrant 5 to 5: ",dictNext_Quad['5'][4]/totalNext[3])
print("Total Percent of Quadrant 5 to 6: ",dictNext_Quad['5'][5]/totalNext[3])
print("Total Percent of Quadrant 5 to 7: ",dictNext_Quad['5'][6]/totalNext[3])
print("Total Percent of Quadrant 5 to 8: ",dictNext_Quad['5'][7]/totalNext[3])

print("Total Percent of Quadrant 6 to 1: ",dictNext_Quad['6'][0]/totalNext[3])
print("Total Percent of Quadrant 6 to 2: ",dictNext_Quad['6'][1]/totalNext[3])
print("Total Percent of Quadrant 6 to 3: ",dictNext_Quad['6'][2]/totalNext[3])
print("Total Percent of Quadrant 6 to 4: ",dictNext_Quad['6'][3]/totalNext[3])
print("Total Percent of Quadrant 6 to 5: ",dictNext_Quad['6'][4]/totalNext[3])
print("Total Percent of Quadrant 6 to 6: ",dictNext_Quad['6'][5]/totalNext[3])
print("Total Percent of Quadrant 6 to 7: ",dictNext_Quad['6'][6]/totalNext[3])
print("Total Percent of Quadrant 6 to 8: ",dictNext_Quad['6'][7]/totalNext[3])

print("Total Percent of Quadrant 7 to 1: ",dictNext_Quad['7'][0]/totalNext[3])
print("Total Percent of Quadrant 7 to 2: ",dictNext_Quad['7'][1]/totalNext[3])
print("Total Percent of Quadrant 7 to 3: ",dictNext_Quad['7'][2]/totalNext[3])
print("Total Percent of Quadrant 7 to 4: ",dictNext_Quad['7'][3]/totalNext[3])
print("Total Percent of Quadrant 7 to 5: ",dictNext_Quad['7'][4]/totalNext[3])
print("Total Percent of Quadrant 7 to 6: ",dictNext_Quad['7'][5]/totalNext[3])
print("Total Percent of Quadrant 7 to 7: ",dictNext_Quad['7'][6]/totalNext[3])
print("Total Percent of Quadrant 7 to 8: ",dictNext_Quad['7'][7]/totalNext[3])

print("Total Percent of Quadrant 8 to 1: ",dictNext_Quad['8'][0]/totalNext[3])
print("Total Percent of Quadrant 8 to 2: ",dictNext_Quad['8'][1]/totalNext[3])
print("Total Percent of Quadrant 8 to 3: ",dictNext_Quad['8'][2]/totalNext[3])
print("Total Percent of Quadrant 8 to 4: ",dictNext_Quad['8'][3]/totalNext[3])
print("Total Percent of Quadrant 8 to 5: ",dictNext_Quad['8'][4]/totalNext[3])
print("Total Percent of Quadrant 8 to 6: ",dictNext_Quad['8'][5]/totalNext[3])
print("Total Percent of Quadrant 8 to 7: ",dictNext_Quad['8'][6]/totalNext[3])
print("Total Percent of Quadrant 8 to 8: ",dictNext_Quad['8'][7]/totalNext[3])

#Total First and Ending
print("Total Start_End Quadrant 1 to 1: ",dictFirst_End['1'][0])
print("Total Start_End Quadrant 1 to 2: ",dictFirst_End['1'][1])
print("Total Start_End Quadrant 1 to 3: ",dictFirst_End['1'][2])
print("Total Start_End Quadrant 1 to 4: ",dictFirst_End['1'][3])
print("Total Start_End Quadrant 1 to 5: ",dictFirst_End['1'][4])
print("Total Start_End Quadrant 1 to 6: ",dictFirst_End['1'][5])
print("Total Start_End Quadrant 1 to 7: ",dictFirst_End['1'][6])
print("Total Start_End Quadrant 1 to 8: ",dictFirst_End['1'][7])

print("Total Start_End Quadrant 2 to 1: ",dictFirst_End['2'][0])
print("Total Start_End Quadrant 2 to 2: ",dictFirst_End['2'][1])
print("Total Start_End Quadrant 2 to 3: ",dictFirst_End['2'][2])
print("Total Start_End Quadrant 2 to 4: ",dictFirst_End['2'][3])
print("Total Start_End Quadrant 2 to 5: ",dictFirst_End['2'][4])
print("Total Start_End Quadrant 2 to 6: ",dictFirst_End['2'][5])
print("Total Start_End Quadrant 2 to 7: ",dictFirst_End['2'][6])
print("Total Start_End Quadrant 2 to 8: ",dictFirst_End['2'][7])

print("Total Start_End Quadrant 3 to 1: ",dictFirst_End['3'][0])
print("Total Start_End Quadrant 3 to 2: ",dictFirst_End['3'][1])
print("Total Start_End Quadrant 3 to 3: ",dictFirst_End['3'][2])
print("Total Start_End Quadrant 3 to 4: ",dictFirst_End['3'][3])
print("Total Start_End Quadrant 3 to 1: ",dictFirst_End['3'][4])
print("Total Start_End Quadrant 3 to 2: ",dictFirst_End['3'][5])
print("Total Start_End Quadrant 3 to 3: ",dictFirst_End['3'][6])
print("Total Start_End Quadrant 3 to 4: ",dictFirst_End['3'][7])

print("Total Start_End Quadrant 4 to 1: ",dictFirst_End['4'][0])
print("Total Start_End Quadrant 4 to 2: ",dictFirst_End['4'][1])
print("Total Start_End Quadrant 4 to 3: ",dictFirst_End['4'][2])
print("Total Start_End Quadrant 4 to 4: ",dictFirst_End['4'][3])
print("Total Start_End Quadrant 4 to 5: ",dictFirst_End['4'][4])
print("Total Start_End Quadrant 4 to 6: ",dictFirst_End['4'][5])
print("Total Start_End Quadrant 4 to 7: ",dictFirst_End['4'][6])
print("Total Start_End Quadrant 4 to 8: ",dictFirst_End['4'][7])

print("Total Start_End Quadrant 5 to 1: ",dictFirst_End['5'][0])
print("Total Start_End Quadrant 5 to 2: ",dictFirst_End['5'][1])
print("Total Start_End Quadrant 5 to 3: ",dictFirst_End['5'][2])
print("Total Start_End Quadrant 5 to 4: ",dictFirst_End['5'][3])
print("Total Start_End Quadrant 5 to 5: ",dictFirst_End['5'][4])
print("Total Start_End Quadrant 5 to 6: ",dictFirst_End['5'][5])
print("Total Start_End Quadrant 5 to 7: ",dictFirst_End['5'][6])
print("Total Start_End Quadrant 5 to 8: ",dictFirst_End['5'][7])

print("Total Start_End Quadrant 6 to 1: ",dictFirst_End['6'][0])
print("Total Start_End Quadrant 6 to 2: ",dictFirst_End['6'][1])
print("Total Start_End Quadrant 6 to 3: ",dictFirst_End['6'][2])
print("Total Start_End Quadrant 6 to 4: ",dictFirst_End['6'][3])
print("Total Start_End Quadrant 6 to 5: ",dictFirst_End['6'][4])
print("Total Start_End Quadrant 6 to 6: ",dictFirst_End['6'][5])
print("Total Start_End Quadrant 6 to 7: ",dictFirst_End['6'][6])
print("Total Start_End Quadrant 6 to 8: ",dictFirst_End['6'][7])

print("Total Start_End Quadrant 7 to 1: ",dictFirst_End['7'][0])
print("Total Start_End Quadrant 7 to 2: ",dictFirst_End['7'][1])
print("Total Start_End Quadrant 7 to 3: ",dictFirst_End['7'][2])
print("Total Start_End Quadrant 7 to 4: ",dictFirst_End['7'][3])
print("Total Start_End Quadrant 7 to 5: ",dictFirst_End['7'][4])
print("Total Start_End Quadrant 7 to 6: ",dictFirst_End['7'][5])
print("Total Start_End Quadrant 7 to 7: ",dictFirst_End['7'][6])
print("Total Start_End Quadrant 7 to 8: ",dictFirst_End['7'][7])

print("Total Start_End Quadrant 8 to 1: ",dictFirst_End['8'][0])
print("Total Start_End Quadrant 8 to 2: ",dictFirst_End['8'][1])
print("Total Start_End Quadrant 8 to 3: ",dictFirst_End['8'][2])
print("Total Start_End Quadrant 8 to 4: ",dictFirst_End['8'][3])
print("Total Start_End Quadrant 8 to 5: ",dictFirst_End['8'][4])
print("Total Start_End Quadrant 8 to 6: ",dictFirst_End['8'][5])
print("Total Start_End Quadrant 8 to 7: ",dictFirst_End['8'][6])
print("Total Start_End Quadrant 8 to 8: ",dictFirst_End['8'][7])

print("Total Percent of Start_End Quadrant 1 to 1: ",dictFirst_End['1'][0]/totalFnEn[0])
print("Total Percent of Start_End Quadrant 1 to 2: ",dictFirst_End['1'][1]/totalFnEn[0])
print("Total Percent of Start_End Quadrant 1 to 3: ",dictFirst_End['1'][2]/totalFnEn[0])
print("Total Percent of Start_End Quadrant 1 to 4: ",dictFirst_End['1'][3]/totalFnEn[0])
print("Total Percent of Start_End Quadrant 1 to 5: ",dictFirst_End['1'][4]/totalFnEn[0])
print("Total Percent of Start_End Quadrant 1 to 6: ",dictFirst_End['1'][5]/totalFnEn[0])
print("Total Percent of Start_End Quadrant 1 to 7: ",dictFirst_End['1'][6]/totalFnEn[0])
print("Total Percent of Start_End Quadrant 1 to 8: ",dictFirst_End['1'][7]/totalFnEn[0])

print("Total Percent of Start_End Quadrant 2 to 1: ",dictFirst_End['2'][0]/totalFnEn[1])
print("Total Percent of Start_End Quadrant 2 to 2: ",dictFirst_End['2'][1]/totalFnEn[1])
print("Total Percent of Start_End Quadrant 2 to 3: ",dictFirst_End['2'][2]/totalFnEn[1])
print("Total Percent of Start_End Quadrant 2 to 4: ",dictFirst_End['2'][3]/totalFnEn[1])
print("Total Percent of Start_End Quadrant 2 to 5: ",dictFirst_End['2'][4]/totalFnEn[1])
print("Total Percent of Start_End Quadrant 2 to 6: ",dictFirst_End['2'][5]/totalFnEn[1])
print("Total Percent of Start_End Quadrant 2 to 7: ",dictFirst_End['2'][6]/totalFnEn[1])
print("Total Percent of Start_End Quadrant 2 to 8: ",dictFirst_End['2'][7]/totalFnEn[1])

print("Total Percent of Start_End Quadrant 3 to 1: ",dictFirst_End['3'][0]/totalFnEn[2])
print("Total Percent of Start_End Quadrant 3 to 2: ",dictFirst_End['3'][1]/totalFnEn[2])
print("Total Percent of Start_End Quadrant 3 to 3: ",dictFirst_End['3'][2]/totalFnEn[2])
print("Total Percent of Start_End Quadrant 3 to 4: ",dictFirst_End['3'][3]/totalFnEn[2])
print("Total Percent of Start_End Quadrant 3 to 5: ",dictFirst_End['3'][4]/totalFnEn[2])
print("Total Percent of Start_End Quadrant 3 to 6: ",dictFirst_End['3'][5]/totalFnEn[2])
print("Total Percent of Start_End Quadrant 3 to 7: ",dictFirst_End['3'][6]/totalFnEn[2])
print("Total Percent of Start_End Quadrant 3 to 8: ",dictFirst_End['3'][7]/totalFnEn[2])

print("Total Percent of Start_End Quadrant 4 to 1: ",dictFirst_End['4'][0]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 4 to 2: ",dictFirst_End['4'][1]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 4 to 3: ",dictFirst_End['4'][2]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 4 to 4: ",dictFirst_End['4'][3]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 4 to 5: ",dictFirst_End['4'][4]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 4 to 6: ",dictFirst_End['4'][5]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 4 to 7: ",dictFirst_End['4'][6]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 4 to 8: ",dictFirst_End['4'][7]/totalFnEn[3])

print("Total Percent of Start_End Quadrant 5 to 1: ",dictFirst_End['5'][0]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 5 to 2: ",dictFirst_End['5'][1]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 5 to 3: ",dictFirst_End['5'][2]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 5 to 4: ",dictFirst_End['5'][3]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 5 to 5: ",dictFirst_End['5'][4]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 5 to 6: ",dictFirst_End['5'][5]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 5 to 7: ",dictFirst_End['5'][6]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 5 to 8: ",dictFirst_End['5'][7]/totalFnEn[3])

print("Total Percent of Start_End Quadrant 6 to 1: ",dictFirst_End['6'][0]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 6 to 2: ",dictFirst_End['6'][1]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 6 to 3: ",dictFirst_End['6'][2]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 6 to 4: ",dictFirst_End['6'][3]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 6 to 5: ",dictFirst_End['6'][4]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 6 to 6: ",dictFirst_End['6'][5]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 6 to 7: ",dictFirst_End['6'][6]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 6 to 8: ",dictFirst_End['6'][7]/totalFnEn[3])

print("Total Percent of Start_End Quadrant 7 to 1: ",dictFirst_End['7'][0]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 7 to 2: ",dictFirst_End['7'][1]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 7 to 3: ",dictFirst_End['7'][2]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 7 to 4: ",dictFirst_End['7'][3]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 7 to 5: ",dictFirst_End['7'][4]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 7 to 6: ",dictFirst_End['7'][5]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 7 to 7: ",dictFirst_End['7'][6]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 7 to 8: ",dictFirst_End['7'][7]/totalFnEn[3])

print("Total Percent of Start_End Quadrant 8 to 1: ",dictFirst_End['8'][0]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 8 to 2: ",dictFirst_End['8'][1]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 8 to 3: ",dictFirst_End['8'][2]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 8 to 4: ",dictFirst_End['8'][3]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 8 to 5: ",dictFirst_End['8'][4]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 8 to 6: ",dictFirst_End['8'][5]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 8 to 7: ",dictFirst_End['8'][6]/totalFnEn[3])
print("Total Percent of Start_End Quadrant 8 to 8: ",dictFirst_End['8'][7]/totalFnEn[3])
