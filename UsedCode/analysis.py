### Ethan Jones
### October 28th, 2019
### The following program is used to analyze passwords and  locate if specific trends exist
### Between key placement on a keyboard, and selection of passwords
# First we will get passwords, and find the average percentage of quadrants 1 & 2 on each password ex: on average passwords contained 40% use of Q1 and 60% use of Q2
# Then we will find most percentage use of Q1 and Q2 in sections of passwords, buy putting them in half, then thirds, then 4ths
# We will then look for percentage of specific letterings and qudrants coming after specific key strokes
# Goal in mind is to find any correlation between ergonomics and passwords, then use supervised learning to creat a bot that will look at some of your passwords
# Then it will try to guess a password you chose.

#Dictionary used for placing key strokes in 2 quadrants then move to 3 quadrants to n quadrants with evolution of project
##Progress


#Hard Coded Because I was still unsure how to use dictionaries at the very beginning of this project
Quadrants = {'`':1, '~':1, 'q': 1, 'a': 1, 'z': 1, 'w':1, 's': 1, 'x': 1, 'e' : 1, 'd': 1,'c': 1, 'r': 1, 'f': 1, 'v':1, 't':1, 'g':1, 'b':1, '1':1, '2':1, '3':1, '4':1, '5':1, \
'!':1, '@':1, '#':1, '$':1, '%':1, 'Q':1,'A':1, 'Z':1, 'W':1, 'S':1, 'X':1, 'E':1, 'D':1, 'C':1, 'R':1, 'F':1, 'V':1, 'T':1, 'G':1, 'B':1, 'y':2,'h':2, 'n':2, 'u':2, 'j':2, 'm':2,\
'i':2, 'k':2, ',':2, 'o':2, 'l':2, '.':2, 'p':2, ';':2, '/':2, '[':2, ']':2, '\\':2, '\'':2, '6':2, '7':2, '8':2, '9':2, '0':2, '-':2, '=':2, '^':2, '&':2, '*':2, '(':2, ')':2,\
'_':2, '+':2, 'Y':2, 'H':2, 'N':2, 'U':2, 'J':2, 'M':2, 'I':2, 'K':2, '<':2, 'O':2, 'L':2, '>':2, 'P':2, ':':2, '?':2, '{':2, '"':2, '}':2, '|':2, ' ':2}

passwords = open('allPass.txt', 'r', encoding = 'latin1')
#passwords_q0 = open('password_Q0.txt', 'w+')
#passwords_q1 = open('password_Q1.txt', 'w+')
#passwords_qe = open('password_QE.txt', 'w+')
#quad_data    = open('quad_data.txt', 'w+')
pwdList = passwords.readlines()

#Number of passwords where Quadrant 1 is more prominent than Quadrant 2
Q_0 = 0
#Number of passwords where Quadrant 2 is more prominent than Quadrant 1
Q_1 = 0
#Number of passwords of equal quadrants
Q_E = 0


#new line character was causing issues
#Cycle through the characters of a password
#Add to more1 if it was from Q1, more2 Q2, and use these variable to derive statistics
#On only two quadrants
for  x in range(len(pwdList)):
    tring = pwdList[x]
    more1 = 0
    more2 = 0

    for j in tring:
        try:
            if(j == '\n'):
                continue
            if Quadrants[j] == 1:
                more1 = more1 + 1
            if Quadrants[j] == 2:
                more2 = more2 + 1
        except:
            i = 0
    qs = (str(more1) + " | " + str(more2))
    #quad_data.write(qs+"\n")
    if more1 > more2:
        Q_0 = Q_0 + 1
        #passwords_q0.write(tring)
    elif more2 > more1:
        Q_1 = Q_1 +1
        #passwords_q1.write(tring)
    else:
        Q_E = Q_E + 1
        #passwords_qe.write(tring)
#Percent of Q_0 dominated passwords
pq0 = (Q_0 / len(pwdList)) * 100

#Percent of Q_1 dominated passwords
pq1 = (Q_1 / len(pwdList)) * 100

#Percent of Q_E passwords
pqe = (Q_E / len(pwdList)) * 100

print("Total number of Q0: ",Q_0)
print("Total number of Q1: ",Q_1)
print("Total number of QE: ",Q_E)
print("Percent of passwords with more Q1 characters: ",pq0)
print("Percent of passwords with more Q2 characters: ",pq1)
print("Percent of passwords with Equal Q1 and Q1   : ",pqe)

passwords.close()
'''
passwords_q0.close()
passwords_q1.close()
passwords_qe.close()
quad_data.close()
'''
print("Dictionary is good")
