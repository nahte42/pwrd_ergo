### Ethan Jones
### October 28th, 2019
### The following program is used to analyze passwords and  locate if specific trends exist
### Between key placement on a keyboard, and selection of passwords
# First we will get passwords, and find the average percentage of quadrants 1 & 2 on each password ex: on average passwords contained 40% use of Q1 and 60% use of Q2
# Then we will find most percentage use of Q1 and Q2 in sections of passwords, buy putting them in half, then thirds, then 4ths
# We will then look for percentage of specific letterings and qudrants coming after specific key strokes
# Goal in mind is to find any correlation between ergonomics and passwords, then use supervised learning to creat a bot that will look at some of your passwords
# Then it will try to guess a password you chose.

#Dictionary used for placing key strokes in 2 quadrants then move to 3 quadrants to n quadrants with evolution of project
##Progress

Quadrants = {'`':1, '~':1, 'q': 1, 'a': 1, 'z': 1, 'w':1, 's': 1, 'x': 1, 'e' : 1, 'd': 1,'c': 1, 'r': 1, 'f': 1, 'v':1, 't':1, 'g':1, 'b':1, '1':1, '2':1, '3':1, '4':1, '5':1, \
'!':1, '@':1, '#':1, '$':1, '%':1, 'Q':1,'A':1, 'Z':1, 'W':1, 'S':1, 'X':1, 'E':1, 'D':1, 'C':1, 'R':1, 'F':1, 'V':1, 'T':1, 'G':1, 'B':1, 'y':2,'h':2, 'n':2, 'u':2, 'j':2, 'm':2,\
'i':2, 'k':2, ',':2, 'o':2, 'l':2, '.':2, 'p':2, ';':2, '/':2, '[':2, ']':2, '\\':2, '\'':2, '6':2, '7':2, '8':2, '9':2, '0':2, '-':2, '=':2, '^':2, '&':2, '*':2, '(':2, ')':2,\
'_':2, '+':2, 'Y':2, 'H':2, 'N':2, 'U':2, 'J':2, 'M':2, 'I':2, 'K':2, '<':2, 'O':2, 'L':2, '>':2, 'P':2, ':':2, '?':2, '{':2, '"':2, '}':2, '|':2, ' ':2}

passwords = open("allPass.txt", "r", encoding="latin1")
#new_pass = open('new_pass.txt', 'w+')
pwdList = passwords.readlines()



#Find the percentage of starting Q1 and Q2 and Ending Q1 and Q2
start_Q1 = 0
start_Q2 = 0
end_Q1 = 0
end_Q2 = 0
same = 0
differ = 0
for x in pwdList:
    try:
        if x == '\n':
            continue
        if Quadrants[x[0]] == 1:
            start_Q1 = start_Q1 + 1
        else:
            start_Q2 = start_Q2 + 1
        if Quadrants[x[::-1][1]] == 1:
            end_Q1 = end_Q1 + 1
        else:
            end_Q2 = end_Q2 + 1
        if (Quadrants[x[0]] == 1 and Quadrants[x[::-1][1]] == 1):
            same = same + 1
        else:
            differ = differ + 1
    except:
        print("", end = "")
#Separate psswords into lists of two halves
#Then find on a halved scale what percentage of each are in passwords

halved = []
for x in pwdList:
    var = int(len(x)/2)
    temp = [x[0:var+1], x[var+1:]]
    halved.append(temp)

frst_half_Q1 = 0
scnd_half_Q1 = 0
frst_half_Q2 = 0
scnd_half_Q2 = 0
same_half_1  = 0
same_half_2  = 0
more1 = more2 = 0
for list in halved:
    strng = list[0]
    for ch in strng:
        try:
            if ch == '\n':
                continue
            if Quadrants[ch] == 1:
                more1 = more1 + 1
            else:
                more2 = more2 + 1
        except:
            i = 0
    if more1 == more2:
        same_half_1 = same_half_1 + 1
    elif more1 > more2:
        frst_half_Q1 = frst_half_Q1 + 1
    else:
        frst_half_Q2 = frst_half_Q2 + 1
    more1 = 0
    more2 = 0

for list in halved:
    strng = list[1]
    for ch in strng:
        try:
            if ch == '\n':
                continue
            if Quadrants[ch] == 1:
                more1 = more1 + 1
            else:
                more2 = more2 + 1
        except:
            i = 0
    if more1 == more2:
        same_half_2 = same_half_2 + 1
    elif more1 > more2:
        scnd_half_Q1 = scnd_half_Q1 + 1
    else:
        scnd_half_Q2 = scnd_half_Q2 + 1
    more1 = 0
    more2 = 0


total = len(pwdList)

print("Percantage of passwords starting in Quadrant 1              : ", (start_Q1/total)*100)
print("Percantage of passwords starting in Quadrant 2              : ", (start_Q2/total)*100)
print("Percantage of passwords ending in Quadrant 1                : ", (end_Q1/total)*100)
print("Percantage of passwords ending in Quadrant 2                : ", (end_Q2/total)*100)
print("Percantage of passwords starting and ending in same quadrant: ", (same/total)*100)
print("Percantage of passwords starting and ending in diff quadrant: ", (differ/total)*100)

print("Percentage of Q1 in first half of passwords                 : ", (frst_half_Q1/total)*100)
print("Percentage of Q2 in first half of passwords                 : ", (frst_half_Q2/total)*100)
print("Percentage of Q1 in second half of passwords                : ", (scnd_half_Q1/total)*100)
print("Percentage of Q2 in second half of passwords                : ", (scnd_half_Q2/total)*100)
print("Percentage of Q1 & Q2 in first half of passwords            : ", (same_half_1/total)*100)
print("Percentage of Q1 & Q2 in second half of passwords           : ", (same_half_2/total)*100)


print("Total # of end_Q1   : ", end_Q1)
print("Total # of end_Q2   : ", end_Q2)
print("Total # of start_Q1 : ", start_Q1)
print("Total # of start_Q2 : ", start_Q2)
print("Total # of same     : ", same)
print("Total # of differ   : ", differ)

print("Total # frst_half_Q1: ", frst_half_Q1)
print("Total # frst_half_Q2: ", frst_half_Q2)
print("Total # scnd_half_Q1: ", scnd_half_Q1)
print("Total # scnd_half_Q2: ", scnd_half_Q2)
print("Total # same_half_1 : ", same_half_1)
print("Total # same_half_2 : ", same_half_2)

print("Total # of passwords: ", total)
