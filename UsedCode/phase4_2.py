#Finding likliness of being in sections for
#8 quadrants
#Extension of phase 4 - Slight copy of Phase 3_2

in_first_section = {'1': 0, '2':0, '3':0, '4':0,'5': 0, '6':0, '7':0, '8':0}

in_scond_section = {'1': 0, '2':0, '3':0, '4':0,'5': 0, '6':0, '7':0, '8':0}

in_third_section = {'1': 0, '2':0, '3':0, '4':0,'5': 0, '6':0, '7':0, '8':0}

in_fourt_section = {'1': 0, '2':0, '3':0, '4':0,'5': 0, '6':0, '7':0, '8':0}

pattern4 = open('allPatt8.txt', 'r')
pattern_list = pattern4.readlines()

section_1 = []
section_2 = []
section_3 = []
section_4 = []


for pattern in pattern_list:
    size_of_pattern = int(len(pattern))
    if(size_of_pattern < 4):
        continue
    number_of_sections = int(size_of_pattern / 4)
    if(size_of_pattern % 4 >= 2 ):
        if size_of_pattern == 6:
            number_of_sections = 1
        else:
            number_of_sections = number_of_sections = 1
    section_1.append(pattern[0: number_of_sections])
    section_2.append(pattern[number_of_sections: number_of_sections + number_of_sections])
    section_3.append(pattern[number_of_sections + number_of_sections: number_of_sections + number_of_sections + number_of_sections])
    section_4.append(pattern[number_of_sections + number_of_sections + number_of_sections:])


for part in section_1:
    for character in part:
        in_first_section[character] =  in_first_section[character] + 1

for part in section_2:
    for character in part:
        in_scond_section[character] =  in_scond_section[character] + 1

for part in section_3:
    for character in part:
        in_third_section[character] =  in_third_section[character] + 1

for part in section_4:
    for character in part:
        if character == '\n':
            continue
        in_fourt_section[character] =  in_fourt_section[character] + 1


total1 = in_first_section['1'] + in_scond_section['1'] + in_third_section['1'] + in_fourt_section['1'] 

total2 = in_first_section['2'] + in_scond_section['2'] + in_third_section['2'] + in_fourt_section['2'] 

total3 = in_first_section['3'] + in_scond_section['3'] + in_third_section['3'] + in_fourt_section['3']

total4 = in_fourt_section['4'] + in_scond_section['4'] + in_third_section['4'] + in_fourt_section['4']

total5 = in_first_section['5'] + in_scond_section['5'] + in_third_section['5'] + in_fourt_section['5']

total6 = in_first_section['6'] + in_scond_section['6'] + in_third_section['6'] + in_fourt_section['6']

total7 = in_first_section['7'] + in_scond_section['7'] + in_third_section['7'] + in_fourt_section['7']

total8 = in_first_section['8'] + in_scond_section['8'] + in_third_section['8'] + in_fourt_section['8']

print("Total amount of quadrant 1 in part 1: ", in_first_section['1'])
print("Total amount of quadrant 2 in part 1: ", in_first_section['2'])
print("Total amount of quadrant 3 in part 1: ", in_first_section['3'])
print("Total amount of quadrant 4 in part 1: ", in_first_section['4'])
print("Total amount of quadrant 5 in part 1: ", in_first_section['5'])
print("Total amount of quadrant 6 in part 1: ", in_first_section['6'])
print("Total amount of quadrant 7 in part 1: ", in_first_section['7'])
print("Total amount of quadrant 8 in part 1: ", in_first_section['8'])

print("Total amount of quadrant 1 in part 2: ", in_scond_section['1'])
print("Total amount of quadrant 2 in part 2: ", in_scond_section['2'])
print("Total amount of quadrant 3 in part 2: ", in_scond_section['3'])
print("Total amount of quadrant 4 in part 2: ", in_scond_section['4'])
print("Total amount of quadrant 5 in part 2: ", in_scond_section['5'])
print("Total amount of quadrant 6 in part 2: ", in_scond_section['6'])
print("Total amount of quadrant 7 in part 2: ", in_scond_section['7'])
print("Total amount of quadrant 8 in part 2: ", in_scond_section['8'])

print("Total amount of quadrant 1 in part 3: ", in_third_section['1'])
print("Total amount of quadrant 2 in part 3: ", in_third_section['2'])
print("Total amount of quadrant 3 in part 3: ", in_third_section['3'])
print("Total amount of quadrant 4 in part 3: ", in_third_section['4'])
print("Total amount of quadrant 5 in part 3: ", in_third_section['5'])
print("Total amount of quadrant 6 in part 3: ", in_third_section['6'])
print("Total amount of quadrant 7 in part 3: ", in_third_section['7'])
print("Total amount of quadrant 8 in part 3: ", in_third_section['8'])

print("Total amount of quadrant 1 in part 4: ", in_fourt_section['1'])
print("Total amount of quadrant 2 in part 4: ", in_fourt_section['2'])
print("Total amount of quadrant 3 in part 4: ", in_fourt_section['3'])
print("Total amount of quadrant 4 in part 4: ", in_fourt_section['4'])
print("Total amount of quadrant 5 in part 4: ", in_fourt_section['5'])
print("Total amount of quadrant 6 in part 4: ", in_fourt_section['6'])
print("Total amount of quadrant 7 in part 4: ", in_fourt_section['7'])
print("Total amount of quadrant 8 in part 4: ", in_fourt_section['8'])

print("Total % amount of quadrant 1 in part 1: ", in_first_section['1']/total1)
print("Total % amount of quadrant 2 in part 1: ", in_first_section['2']/total2)
print("Total % amount of quadrant 3 in part 1: ", in_first_section['3']/total3)
print("Total % amount of quadrant 4 in part 1: ", in_first_section['4']/total4)
print("Total % amount of quadrant 5 in part 1: ", in_first_section['5']/total5)
print("Total % amount of quadrant 6 in part 1: ", in_first_section['6']/total6)
print("Total % amount of quadrant 7 in part 1: ", in_first_section['7']/total7)
print("Total % amount of quadrant 8 in part 1: ", in_first_section['8']/total8)

print("Total % amount of quadrant 1 in part 2: ", in_scond_section['1']/total1)
print("Total % amount of quadrant 2 in part 2: ", in_scond_section['2']/total2)
print("Total % amount of quadrant 3 in part 2: ", in_scond_section['3']/total3)
print("Total % amount of quadrant 4 in part 2: ", in_scond_section['4']/total4)
print("Total % amount of quadrant 5 in part 2: ", in_scond_section['5']/total5)
print("Total % amount of quadrant 6 in part 2: ", in_scond_section['6']/total6)
print("Total % amount of quadrant 7 in part 2: ", in_scond_section['7']/total7)
print("Total % amount of quadrant 8 in part 2: ", in_scond_section['8']/total8)

print("Total % amount of quadrant 1 in part 3: ", in_third_section['1']/total1)
print("Total % amount of quadrant 2 in part 3: ", in_third_section['2']/total2)
print("Total % amount of quadrant 3 in part 3: ", in_third_section['3']/total3)
print("Total % amount of quadrant 4 in part 3: ", in_third_section['4']/total4)
print("Total % amount of quadrant 5 in part 3: ", in_third_section['5']/total5)
print("Total % amount of quadrant 6 in part 3: ", in_third_section['6']/total6)
print("Total % amount of quadrant 7 in part 3: ", in_third_section['7']/total7)
print("Total % amount of quadrant 8 in part 3: ", in_third_section['8']/total8)

print("Total % amount of quadrant 1 in part 4: ", in_fourt_section['1']/total1)
print("Total % amount of quadrant 2 in part 4: ", in_fourt_section['2']/total2)
print("Total % amount of quadrant 3 in part 4: ", in_fourt_section['3']/total3)
print("Total % amount of quadrant 4 in part 4: ", in_fourt_section['4']/total4)
print("Total % amount of quadrant 5 in part 4: ", in_fourt_section['5']/total5)
print("Total % amount of quadrant 6 in part 4: ", in_fourt_section['6']/total6)
print("Total % amount of quadrant 7 in part 4: ", in_fourt_section['7']/total7)
print("Total % amount of quadrant 8 in part 4: ", in_fourt_section['8']/total8)
